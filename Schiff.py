# -*- coding: utf-8 -*-
"""
Created on Mon Sep  6 11:54:58 2021

@author: Oskar Schluttig
"""

class Schiff:
    def __init__(self, _id = None, name = "", kapazitaet = 100, user = None):
        self.id = _id
        self.name = name
        self.kapazitaet = kapazitaet
        self.user = user
    