from Schiff import Schiff
import collections

class AlgorithmusConstanze():

    # Konstruktor
    def __init__(self, ressource = 0, Schiff = None):

        self.schiffe = []
        self.sortierte_schiffe = dict()
        self.kapazitaet = 0
        self.anzahl = 0
        self.ressource = ressource
        for s in Schiff:
            self.anzahl +=1
            self.schiffe.append(s)
            self.sortierte_schiffe[s.kapazitaet] = s
            self.kapazitaet += s.kapazitaet

        self.sortierte_schiffe_result = collections.OrderedDict(sorted(self.sortierte_schiffe.items(), reverse=True))

    # Zeigt alle Schiffe aus der Datenbank
    def getSchiffBestand(self):
        text = "VORHANDENE SCHIFFE:\n"
        for s in self.schiffe:
            text += "Schiff " + s.name + " mit Kapazitaet: " + str(s.kapazitaet) + " EH \n"
        return text

    # Zeitkritische Berechnung
    def zeitkritisch(self):
        counter = 1
        flag = True
        text = "ZEITKRITISCH:\n"
        tempRessource = self.ressource
        while flag:
            if tempRessource <= counter * self.kapazitaet:
                for s in self.schiffe:
                    tempRessource -= s.kapazitaet
                flag = False
            else:
                counter += 1

        freie_kapazitaet = counter * self.kapazitaet - self.ressource
        text += "Es ist/sind " + str(counter) + " Runde(n) mit allen Schiffen nötig und es bleibt bei der letzten, durchgeführten Runde " + str(freie_kapazitaet) + " EH Kapazitaet frei."
        return text

    # Kapazitätskritische Berechnung (Zusammenfürung)
    def kapazitaetkritischGesamt(self, ressourceHolz, ressourceStahl, ressourceFisch):
        text = "KAPAZITAETSKRITISCH:\n"
        textH = self.kapazitaetEinzelSchiff(int(ressourceHolz), "Holz")
        textS = self.kapazitaetEinzelSchiff(int(ressourceStahl), "Stahl")
        textF = self.kapazitaetEinzelSchiff(int(ressourceFisch), "Fisch")

        if textH == "":
            textH = self.regrusiveKapazitaet(int(ressourceHolz), "Holz")

        if textS == "":
            textS = self.regrusiveKapazitaet(int(ressourceStahl), "Stahl")

        if textF == "":
            textF = self.regrusiveKapazitaet(int(ressourceFisch), "Fisch")

        resultText = text + textH + textS + textF

        return resultText

    # Kapazitätskritische Berechnung, wenn Resource genau zu Kapazitat passt
    def kapazitaetEinzelSchiff(self, ressource, ressourcenName):
        text =""
        for s in self.schiffe:
            if int(ressource) == s.kapazitaet:
                text += "Schiff " + s.name + " transportiert " + str(s.kapazitaet) + " EH " + ressourcenName + "\n"
        return text

    # Kapazitätskritische Berechnung, wenn Resource nicht genau zu Kapazitat passt und aufgeteilt werden muss
    def regrusiveKapazitaet(self, ressource, ressourenName):
        text = ""
        for s in self.sortierte_schiffe_result:
            if ressource < int(self.sortierte_schiffe_result.get(s).kapazitaet):
                text = "Schiff " + self.sortierte_schiffe_result.get(s).name + " transportiert " + str(ressource) + " EH " + ressourenName + "\n"
                return text
            if ressource > int(self.sortierte_schiffe_result.get(s).kapazitaet):
                print(self.sortierte_schiffe_result.get(s).kapazitaet)
                diff = ressource - int(self.sortierte_schiffe_result.get(s).kapazitaet)
                text = "Schiff " + self.sortierte_schiffe_result.get(s).name + " transportiert " + str(self.sortierte_schiffe_result.get(s).kapazitaet) + " EH " + ressourenName + "\n"
                if diff > 0:
                    text += self.regrusiveKapazitaet(int(diff), ressourenName)
                else:
                    return text
            return text