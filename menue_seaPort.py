# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import sys
from PyQt5.QtWidgets import QApplication, QFormLayout, QGraphicsDropShadowEffect, QGridLayout, QLabel, QPushButton, QWidget
from PyQt5.QtGui import QImage, QPalette, QBrush, QFont, QColor
from PyQt5.QtCore import QSize, Qt
from questGUI import Quest
from SchiffsWidget import SchiffsMask

class Menue(QWidget):

    def __init__(self, dbc):
        super().__init__()
        
        self.title = 'SeaPort - Toolsoftware'
        self.left = 50
        self.top = 50
        self.width = 640
        self.height = 480
        self.dbc = dbc
        self.initUI()
        
    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        
        # background image
        self.resizeEvent(None)
        
        # title
        self.titleLabel = QLabel("SeaPort\nToolsoftware", self)
        self.titleLabel.setFont(QFont("Times", 30, QFont.Bold))
        self.titleLabel.setAlignment(Qt.AlignCenter)
        self.titleLabel.setStyleSheet("color:black")
        self._addShadowEffect(self.titleLabel)
        
        # buttons
        btn_schiffe = QPushButton('Schiffsdaten eingeben')
        self.buttonGetStyle(btn_schiffe)
        btn_quests = QPushButton('Quest lösen')
        self.buttonGetStyle(btn_quests)
        btn_beenden = QPushButton('Beenden')
        self.buttonGetStyle(btn_beenden)
        
        btn_schiffe.clicked.connect(self.openShips)
        btn_quests.clicked.connect(self.openQuest)
        btn_beenden.clicked.connect(self.closeWindow)
        
        layout = QGridLayout()
        buttons = QWidget()
        buttonsLayout = QFormLayout()
        
        buttonsLayout.addWidget(btn_schiffe)
        buttonsLayout.addWidget(btn_quests)
        buttonsLayout.addWidget(btn_beenden)
        
        buttonsLayout.setVerticalSpacing(20)
        buttonsLayout.setContentsMargins(0, 50, 0, 0)
        
        buttons.setLayout(buttonsLayout)
        layout.addWidget(self.titleLabel, 0, 0, 1, 3)
        layout.addWidget(buttons, 1, 1, 7, 1)

        self.setLayout(layout)

        self.show()
    
    def resizeEvent(self, a0):
        # background image
        oImage = QImage("images/background.jpg")
        sImage = oImage.scaled(QSize(self.frameGeometry().width(), self.frameGeometry().height()))
        palette = QPalette()
        palette.setBrush(10, QBrush(sImage))
        self.setPalette(palette)
    
    def openShips(self):
        self.ships = SchiffsMask(self)
        self.ships.show()
        self.hide()
    
    def openQuest(self):
         self.quest = Quest(self)
         self.quest.show()
         self.hide()
         zurueck = self.quest.zurueck
         #while not zurueck:
          #   print(zurueck)
             #zurueck = self.quest.zurueck
         #self.show()
         #self.quest.close()
        
    def closeWindow(self):
        self.close()
    
    def _addShadowEffect(self, item):
        effect = QGraphicsDropShadowEffect()
        effect.setBlurRadius(1)
        effect.setColor(QColor("white"))
        effect.setOffset(0, 1)
        item.setGraphicsEffect(effect)
        
    def buttonGetStyle(self, button):
        styleSheet = """
QPushButton {
    background-color: #51b9a5; 
    border-radius: 7px; 
    padding: 10px 70px 10px 70px; 
    border: None
}

QPushButton:hover {
    background-color: #64b5f6;
    color: #fff;
}

QPushButton:pressed {
    background-color: #bbdefb;
} """
        button.setFont(QFont("Times", 12))
        button.setStyleSheet(styleSheet)


class SubWindow(QWidget):
     def __init__(self):
         super(SubWindow, self).__init__()
         self.resize(400, 300)

         # Label
         self.label = QLabel(self)
         self.label.setGeometry(0, 0, 400, 300)
         self.label.setText('Sub Window')
         self.label.setAlignment(Qt.AlignCenter)
         self.label.setStyleSheet('font-size:40px')
  
    
if __name__ == '__main__':
    app = QApplication([])
    ex = Menue()
    sys.exit(app.exec_())