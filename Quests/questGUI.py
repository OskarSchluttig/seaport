import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QIcon, QImage, QPalette, QBrush, QFont, QColor
from PyQt5.QtCore import QSize, Qt


class Quest(QWidget):

    def __init__(self):
        super().__init__()

        self.title = 'SeaPort - Quests'
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        self.zurueck = False
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        # background image
        oImage = QImage("background_quests.jpg")
        sImage = oImage.scaled(QSize(640, 480))
        palette = QPalette()
        palette.setBrush(10, QBrush(sImage))
        self.setPalette(palette)

        # title
        self.titleLabel = QLabel("Quests", self)
        self.titleLabel.setFont(QFont("Times", 30, QFont.Bold))
        self.titleLabel.setAlignment(Qt.AlignCenter)
        self.titleLabel.setStyleSheet("color:black")
        self._addShadowEffect(self.titleLabel)


        # Input
        self.headline_input = QLabel()
        self.headline_input.setText("Eingabe der Mengen:")
        self.holz_input = QLineEdit()
        self.stahl_input = QLineEdit()
        self.fisch_input = QLineEdit()

        # Dropdown
        self.dropdown = QComboBox()
        self.dropdown.addItems(["Wähle Person", "Lucy", "Theresa", "Oskar", "Jeremy", "Constanze"])

        # Output
        self.headline_output = QLabel()
        self.headline_output.setText("Berechnete Loesung:")
        self.output = QPlainTextEdit()
        self.output.setReadOnly(True)

        # FormLayout
        self.flo = QFormLayout()
        self.flo.addWidget(self.titleLabel)
        self.flo.addWidget(self.headline_input)
        self.flo.addRow("Holz", self.holz_input)
        self.flo.addRow("Stahl", self.stahl_input)
        self.flo.addRow("Fisch", self.fisch_input)
        self.flo.addWidget(QCheckBox('kapazitaetskritisch'))
        self.flo.addWidget(QCheckBox('zeitkritisch'))
        self.flo.addWidget(self.dropdown)

        # Button Berechne
        self.flo.addWidget(QPushButton('Berechne'))

        self.flo.addWidget(self.headline_output)
        self.flo.addWidget(self.output)
        self.button_zurueck = QPushButton('Zurück zum Hauptmenue')
        self.flo.addWidget(self.button_zurueck)
        self.button_zurueck.clicked.connect(self.setZurueck)

        self.setLayout(self.flo)
        self.show()

    def setZurueck(self):
        self.zurueck = True

    def _addShadowEffect(self, item):
        effect = QGraphicsDropShadowEffect()
        effect.setBlurRadius(1)
        effect.setColor(QColor("white"))
        effect.setOffset(0, 1)
        item.setGraphicsEffect(effect)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Quest()
    sys.exit(app.exec_())
