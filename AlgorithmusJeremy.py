import itertools

from Schiff import Schiff


class Ship:
    def __init__(self, name, capacity):
        self.name = name
        self.capacity: int = capacity

    # Method to represent the object
    # allows easy debugging and printing

    def __repr__(self):
        return "(" + self.name + ")"


class Resource:
    def __init__(self, type, value):
        self.type = type
        self.value: int = value

    # Method to represent the object
    # allows easy debugging and printing

    def __repr__(self):
        return "(" + self.type + ", " + str(self.value) + ")"


def take_capacity(ship: Ship):
    # Method that returns the capacity of a ship
    # Probably not needed
    return ship.capacity


def get_resource_value(resource: Resource):
    # Methode that returns value of a resource
    # Probably not needed
    return resource.value


def get_sum_capacity(list_ship):
    # Method to return sum of capacities from a ship list
    sum_capacity: int = 0
    for ship in list_ship:
        sum_capacity += take_capacity(ship)
    return sum_capacity


def get_sum_resources(list_resource):
    # Method that returns sum of resources
    sum_of_resources: int = 0
    for resource in list_resource:
        sum_of_resources += get_resource_value(resource)
    return sum_of_resources


def clone_schiff_to_ship(list_schiffe: list[Schiff]):
    # Method to build ships as own Ship object
    # To be able to use own string method
    as_ship_list = []
    for ship in list_schiffe:
        as_ship_list.append(Ship(ship.name, ship.kapazitaet))
    return as_ship_list


def clone_ressource_to_own_ressource(array_of_resources):
    # Method to build resources as own Ressource object
    # To be able to use own string method
    name_resources = ["Holz", "Stahl", "Fisch"]
    as_resource_list = []
    for resource in array_of_resources:
        index = array_of_resources.index(resource)
        as_resource_list.append(Resource(name_resources[index], resource))
    return as_resource_list


def return_ship_list_for_capacity(ship_list: list[Ship], sum_resources):
    # Method to return the combination of ships
    # which form the highest sum for that: sum of resources % sum = 0
    # that ensures that the ships will always be at max capacity
    max_sum_to_divide_resources = 0
    array_of_divider_and_max_sum = []
    array_of_divider_ships = []
    # L = length of given array
    # itertools.combination is a fast built method
    # that calculates all combination of elements of an given array for specified length.
    for L in range(0, len(ship_list) + 1):
        for subset in itertools.combinations(ship_list, L):
            tmp_sum = get_sum_capacity(subset)
            if tmp_sum > 0:
                if sum_resources % tmp_sum == 0 and tmp_sum > max_sum_to_divide_resources:
                    max_sum_to_divide_resources = tmp_sum
                    array_of_divider_and_max_sum.clear()
                    array_of_divider_and_max_sum.append([tmp_sum, subset])
                    array_of_divider_ships.clear()
                    array_of_divider_ships.append(subset)
    return array_of_divider_ships


def return_ship_list_for_minimum(ship_list: list[Ship], sum_resources):
    # Method to return the combination of ships
    # which sum will be equal or minimal to a sum of resources.
    # Method to be used in the last round.
    # A sum must be higher or equal to sum of resources are used
    array_of_min_combination_and_actual_difference = []
    combination_of_ships_to_return = []
    # At start set difference to max
    # L = length of given array
    # itertools.combination is a fast built method
    # that calculates all combination of elements of an given array for specified length.
    # At start set difference to max
    difference = sum_resources
    for L in range(0, len(ship_list) + 1):

        for subset in itertools.combinations(ship_list, L):
            tmp_sum = get_sum_capacity(subset)
            if tmp_sum == sum_resources:
                array_of_min_combination_and_actual_difference.clear()
                difference = sum_resources - tmp_sum
                array_of_min_combination_and_actual_difference.append([tmp_sum, subset])
                combination_of_ships_to_return.clear()
                combination_of_ships_to_return.append(subset)
                break
            elif tmp_sum > sum_resources and abs(sum_resources - tmp_sum) < difference:
                array_of_min_combination_and_actual_difference.clear()
                array_of_min_combination_and_actual_difference.append([tmp_sum, subset])
                difference = abs(sum_resources - tmp_sum)
                combination_of_ships_to_return.clear()
                combination_of_ships_to_return.append(subset)
    return combination_of_ships_to_return


def solution_to_string(solution_as_list):
    # Method to return the solution list as String
    solution_as_string = ""
    # The solution list are several list in lists.
    # First each round
    for round_number in solution_as_list:
        solution_as_string += ("Runde" + str(solution_as_list.index(round_number) + 1) + "\n")
        # Each round contains a list of lists of the ship and resources
        for ships_and_ressources in round_number:
            # Prints now out every element in the list, the ship and the resources
            for elem in ships_and_ressources:
                solution_as_string += str(elem) + " "  # solution_as_string += ("/n" + ships_and_ressources)
            solution_as_string += ("\n")
        solution_as_string += ("\n")
    return solution_as_string


def solve_fast(input_ship_list, input_resource_list):
    # Algorithm to give back a solution with a minimum of Rounds
    # In the last Round the empty capacity is minimal

    # change input to own classes
    ship_list = clone_schiff_to_ship(input_ship_list)
    resource_list = clone_ressource_to_own_ressource(input_resource_list)
    # Setting variables
    sum_capacity = get_sum_capacity(ship_list)
    sum_resource = get_sum_resources(resource_list)
    ship_list.sort(key=take_capacity, reverse=True)
    number_rounds = 0
    solution_fast = []

    # Start algorithm until the sum of left resources is null
    while sum_resource > 0:
        if sum_resource > sum_capacity:
            # max capacity
            solution_for_this_round = []
            # Iterate over every ship in the list
            for ship in ship_list:
                # Iterate until the ship is full over every resource of resource list.
                current_resource_index = 0
                # Set the free capacity of the current ship
                free_capacity = ship.capacity
                # Array of resources that the ship will transport this round
                ship_transported_resources = []
                while free_capacity > 0:
                    # Check if current resource is bigger then the ship's capacity
                    if free_capacity < get_resource_value(resource_list[current_resource_index]):
                        resource_list[current_resource_index].value -= free_capacity
                        sum_resource -= free_capacity
                        ship_transported_resources.append(
                            Resource(resource_list[current_resource_index].type, free_capacity))
                        free_capacity = 0
                    else:
                        tmp_resource_value = resource_list[current_resource_index].value
                        resource_list[current_resource_index].value -= get_resource_value(
                            resource_list[current_resource_index])
                        free_capacity -= tmp_resource_value
                        sum_resource -= tmp_resource_value
                        ship_transported_resources.append(
                            Resource(resource_list[current_resource_index].type, tmp_resource_value))
                        current_resource_index += 1
                        # Check for next resource until capacity is full
                solution_for_this_round.append([ship, ship_transported_resources])
            solution_fast.append(solution_for_this_round)
            number_rounds += 1
        else:
            # Now we are in the last round, since the sum of the left resources if smaller
            # then the sum of the capacities of the ships.
            # Calls methode to find combination of ships which will have the smallest amount
            # of not used capacity.
            solution_for_this_round = []
            ship_list_last_round = return_ship_list_for_minimum(ship_list, sum_resource)
            index_ship = 0
            # Iterating until all resources are left
            while sum_resource > 0:
                # Check if sum of resources are higher then capacity of current ship
                if ship_list_last_round[0][index_ship].capacity < sum_resource:
                    ship_transported_resources = []
                    current_resource_index = 0
                    free_capacity = ship_list_last_round[0][index_ship].capacity
                    while free_capacity > 0:
                        if free_capacity < get_resource_value(resource_list[current_resource_index]):
                            resource_list[current_resource_index].value -= free_capacity
                            sum_resource -= free_capacity
                            ship_transported_resources.append(
                                Resource(resource_list[current_resource_index].type, free_capacity))
                            free_capacity = 0
                        else:
                            tmp_resource_value = resource_list[current_resource_index].value
                            resource_list[current_resource_index].value -= resource_list[current_resource_index].value
                            free_capacity -= tmp_resource_value
                            sum_resource -= tmp_resource_value
                            ship_transported_resources.append(
                                Resource(resource_list[current_resource_index].type, tmp_resource_value))
                            current_resource_index += 1
                    solution_for_this_round.append([ship_list_last_round[0][index_ship], ship_transported_resources])
                    index_ship += 1
                else:
                    # Now the last ship to transport resources
                    ship_transported_resources = []
                    while sum_resource > 0:
                        ship_transported_resources = []
                        free_capacity = ship_list_last_round[0][index_ship].capacity
                        for resource in resource_list:
                            tmp_resource_value = resource.value
                            resource.value -= resource.value
                            free_capacity -= tmp_resource_value
                            sum_resource -= tmp_resource_value
                            ship_transported_resources.append(
                                Resource(resource_list[current_resource_index].type, tmp_resource_value))
                        ship_transported_resources.append(
                            Resource("Freie Kapazitaet: ", free_capacity))
                    solution_for_this_round.append([ship_list_last_round[0][index_ship], ship_transported_resources])
            solution_fast.append(solution_for_this_round)
            number_rounds += 1
    return solution_to_string(solution_fast)


def solve_capacity(input_ship_list, input_resource_list):
    # Method to return a solution where every ship is
    # used at max capacity.

    # Clone input into own classes
    ship_list = clone_schiff_to_ship(input_ship_list)
    resource_list = clone_ressource_to_own_ressource(input_resource_list)

    # Idea is to find combination of ships that will ensure always full capacity
    sum_resource = get_sum_resources(resource_list)
    ship_list.sort(key=take_capacity, reverse=True)
    # Getting the list of ships with biggest sum that ensure full capacity
    ship_list_for_max_capacity = return_ship_list_for_capacity(ship_list, sum_resource)
    # If no solution is found, recommendation for other solution method
    if len(ship_list_for_max_capacity) == 0:
        solution_as_list = solve_fast(ship_list, resource_list)
        answer = "No solution found for perfect capacity, returning fast algorithm: " + solution_to_string(
            solution_as_list)
        return answer
    number_rounds = 0
    solution_capacity = []
    # Start algorithm until the sum of left resources is null
    while sum_resource > 0:
        # The ships will always be at max capacity.
        index_ship = 0
        solution_for_this_round = []
        while index_ship < len(ship_list_for_max_capacity[0]):
            ship_transported_resources = []
            current_resource_index = 0
            free_capacity = ship_list_for_max_capacity[0][index_ship].capacity
            while free_capacity > 0:
                if free_capacity < get_resource_value(resource_list[current_resource_index]):
                    resource_list[current_resource_index].value -= free_capacity
                    sum_resource -= free_capacity
                    ship_transported_resources.append(
                        Resource(resource_list[current_resource_index].type, free_capacity))
                    free_capacity = 0
                else:
                    tmp_resource_value = resource_list[current_resource_index].value
                    resource_list[current_resource_index].value -= get_resource_value(
                        resource_list[current_resource_index])
                    free_capacity -= tmp_resource_value
                    sum_resource -= tmp_resource_value
                    ship_transported_resources.append(
                        Resource(resource_list[current_resource_index].type, tmp_resource_value))
                    current_resource_index += 1
            solution_for_this_round.append([ship_list_for_max_capacity[0][index_ship], ship_transported_resources])
            index_ship += 1
        solution_capacity.append(solution_for_this_round)
        number_rounds += 1
    return solution_to_string(solution_capacity)

