# -*- coding: utf-8 -*-
"""
Created on Sat Oct 30 13:13:07 2021

@author: Oskar
"""
# ceil function (round to the next integer)
from math import ceil
# database connection
from TestDBConnection import DBConnection

class AlgorithmOskar:
    def __init__(self, ships):
        self.ships = ships

    @staticmethod
    def __solve(load, ships):
        '''
        Verteilt die Ladung möglichst optimal auf die Schiffe.

        Parameters
        ----------
        load : int
            Ladung, die auf die Schiffe verteilt werden soll.
        ships : list
            Liste mit Schiffen, absteigend sortiert nach Kapazitaet.

        Returns
        -------
        load : int
            Ladung, die nach Verteilung auf die Schiffe noch übrig ist.
            Negative Werte bedeuten, dass noch Entsprechend Platz auf einem der
            Schiffe ist.
        ships : list
            Liste der beladenen Schiffe.

        '''
        # Abbruchbedingungen
        if load <= 0 or len(ships) == 0:
            return load, []
        if load == ships[0].kapazitaet:
            return 0, [ships[0]]
        
        # Nach aufladen der Ressourcen wäre noch Platz auf dem Schiff
        # -> mit dem nächst-kleineren Schiff probieren
        elif load < ships[0].kapazitaet:
            l, s = AlgorithmOskar.__solve(load, ships[1:])
            # falls kein Schiff gefunden wurde, dieses Schiff nehmen
            if len(s) == 0:
                return load - ships[0].kapazitaet, [ships[0]]
            return l, s
        
        # Ladung reicht, das Schiff komplett zu füllen
        else:
            # Restliche Ladung auf restliche Schiffe verteilen
            l, s = AlgorithmOskar.__solve(load - ships[0].kapazitaet, ships[1:])
            # Falls die Lösung nicht optimal ist, versuchen eine bessere
            # Lösung zu finden
            if l < 0:
                l2, s2 = AlgorithmOskar.__solve(load, ships[1:])
                if l2 <= 0 and l2 >= l:
                    return l2, s2
            return l, [ships[0]] + s        
    
    
    def solve_time(self, load):
        # Schiffe nach Kapazität sortieren (größtes zuerst)
        ships = sorted(self.ships, key = lambda x: x.kapazitaet, reverse = True)
        
        # Liste für die Lösung
        rounds = []
        
        # Maximale Kapazität pro Runde
        maxCap = 0
        for s in ships:
            maxCap += s.kapazitaet
        
        # Solange die Ladung größer ist, als die maximale Kapazität pro Runde,
        # können alle Schiffe geschickt werden
        while load > maxCap:
            rounds.append(ships)
            load -= maxCap
        
        # die restliche Ladung wird in einer letzten Runde verteilt
        l, s = AlgorithmOskar.__solve(load, ships)
        rounds.append(s)
        return l, rounds
    
    def solve_capacity(self, load):
        ships = self.ships
        
        # Schiff mit kleinster Kapazität bestimmen
        minShip = min(ships, key=lambda x: x.kapazitaet)
        
        # füge Schiffe so oft hinzu, dass auch das kleinste alle Ressourcen
        # alleine tranportieren könnte (in entsprechend vielen Runden)
        ships *= ceil(load/minShip.kapazitaet)
        
        # Schiffe nach Kapazität sortieren (größtes zuerst)
        ships.sort(key = lambda x: x.kapazitaet, reverse = True)
        
        # eigentlicher Algorithmus als Unterfunktion, die rekursiv aufgerufen wird
        l, s = AlgorithmOskar.__solve(load, ships)
        
        # Schiffe in Runden einteilen
        rounds = []
        while len(s) > 0:
            currentRound = []
            for i in range(len(s)):
                if s[0] not in currentRound:
                    currentRound.append(s.pop(0))
            rounds.append(currentRound)
        
        return l, rounds
    
    @staticmethod
    def __resultToString(load, rounds):
        result = ""
        for i in range(len(rounds)):
            result += "Runde " + str(i+1) + ":"
            for ship in rounds[i]:
                result += " " + ship.name + ","
            result = result[:-1] + "\n"
        result += "Verbleibender Platz: " + str(load) + "\n"
        return result
    
    def solve(self, load, time, capacity):
        result = "Schiffe:\n"
        
        for ship in self.ships:
            result += ship.name + " " + str(ship.kapazitaet) + "\n"
        
        if time:
            l, r = self.solve_time(load)
            result += "\nLösung nach Zeit:\n"
            result += AlgorithmOskar.__resultToString(l, r)
        if capacity:
            l, r = self.solve_capacity(load)
            result += "\nLösung nach Kapazität:\n"
            result += AlgorithmOskar.__resultToString(l, r)
        return result

if __name__=="__main__":
    DBC = DBConnection(
            host="remotemysql.com",
            user="u5LhSJr9zQ",
            password="Mp7ha2LNS5",
            database="u5LhSJr9zQ",
            username="Oskar")
    ships = DBC.getSchiffe()
    
    o = AlgorithmOskar(ships)
    
    load = 510
    print(o.solve(load, True, True))
    
