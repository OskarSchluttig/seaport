# -*- coding: utf-8 -*-
"""
Created on Fri Oct 22 16:07:51 2021

@author: Lucy
"""
import time

class AlgorithmusLucy():
    
    # Vorbereitung der Daten fuer die Algorithmen sowie
    # Weiterleiten der Loesung
    def solveProblem(self, kapazitaet, zeit, ressource, dbc):
        # benoetigte Daten
        ressourcen = []
        array = self.getSchiffe(dbc)
        zahl = 0
        for res in ressource:
            ressourcen.append(int(res))
            zahl += int(res)
        print("Schiffe vorhanden: \t", array)
        print("Ressourcen: \t\t", zahl)
        kombis = self.ermittleKombis(array[0])
        
        # berechne Loesung
        solution = self.trySolveIdea(array[0], self.bereinigeArray(kombis), zahl, kapazitaet, zeit)    
        
        # Loesung wird dem Loesungsstring hinzugefuegt
        if kapazitaet and zeit:
            print("Lösung Zeit:\t\t", solution[0])
            print("Lösung Kapazität:\t", solution[1])
            textSol = "Zeitkritische Lösung:"
            for i in range(0, len(solution)):
                textSol += self.textSolution(solution[i], array, ressourcen)
                if not i == 1:
                    textSol += "\n\nKapazitätskritische Lösung:"
            return textSol
        else:
            if kapazitaet:
                print("Lösung Kapazität:\t", solution[0]) 
                return self.textSolution(solution[0], array, ressourcen)
            elif zeit:
                print("Lösung Zeit:\t\t", solution[0])
                return self.textSolution(solution[0], array, ressourcen)

    # Aufarbeiten der Loesungen in einen String
    def textSolution(self, solution, array, ressource):
        textSolution = ""
        ressourcen = ressource.copy()
        i = 1
        # gehe jede Runde durch
        for rounds in solution:
            # hol die Namen der Schiffe
            ships = self.getShipNames(rounds, array)
            textSolution += "\nRunde " + str(i)+": "
            roundText = ""
            # gehe jedes Schiff durch
            for j in range (0, len(ships)):
                roundText += "\n\tSchiff '"+ships[j]+"' ("+str(rounds[j])+" Kapazität):"
                kap = rounds[j]
                # gehe jede Ressource durch, um anzuzeigen, welches Schiff
                # welche Ressourcen transportiert
                for k in range (0, len(ressourcen)):
                    # falls Kapazitaet groesser/gleich der benoetigten Ressource:
                    # minimiere Kapazitaet um die Ressource und setze Ressource auf 0
                    if kap >= ressourcen[k]:
                        kap -= ressourcen[k]
                        name = " Holz"
                        if k == 1:
                            name = " Stahl"
                        elif k == 2:
                            name = " Fisch"
                        roundText += "\n\t\t"+str(ressourcen[k])+name
                        ressourcen[k] = 0
                    # ansonsten minimiere Ressource um die Kapazitaet und setze
                    # Kapazitaet auf 0
                    elif kap < ressourcen[k]:
                        name = " Holz"
                        if k == 1:
                            name = " Stahl"
                        elif k == 2:
                            name = " Fisch"
                        roundText += "\n\t\t"+str(kap)+name
                        ressourcen[k] -= kap
                        kap = 0
                        
            # fuege Text der Runde dem finalen Text hinzu       
            textSolution += roundText
            textSolution += "\n"
            i += 1
        return textSolution

    
    # ermittle die Namen zu denjeweiligen Schiffen
    def getShipNames(self, ships, array):
        shipNames = []
        oldShipName = ""
        oldIndex = 0
        # gehe jedes Schiff durch
        for ship in ships:
            start = 0
            # ermittle Index eines Schiffes mit der Kapazitaet
            index = array[0].index(ship)
            # falls doppelter Schiffsname, suche erneut nach dem letzten Index
            if array[1][index] == oldShipName:
                start = oldIndex
                index = array[0].index(ship, start)
            # speicher den Schiffsnamen bei dem Index ab
            shipNames.append(array[1][index])
            oldShipName = array[1][index]
            oldIndex = index
        return shipNames
            
    # Schiffe aus der Datenbank ziehen und notwendige Informationen
    # abspeichern
    def getSchiffe(self, dbc):
        data = dbc.getSchiffe()
        array = []
        names = []
        for row in data :
            array.append(row.kapazitaet)
            names.append(row.name)
        return array, names
    
    # ermittle alle moeglichen Kombinationen der Schiffe, die in einer
    # Runde losgeschickt werden koennten
    def ermittleKombis(self, array):
        kombis = []
        # gehe jedes Element durch
        for a in array:
            kombi2 = []
            # alle bisher gefundenen Kombinationen werden um das neue Element
            # addiert und als neue Kombinationen hinzugefuegt
            for k in kombis:
                kCopy = k.copy()
                kCopy.append(a)
                kombi2.append(kCopy)
            for k in kombi2:
                kombis.append(k)
            # das neue Element wird auch hinzugefuegt
            kombis.append([a])
        return kombis
    
    # Kombinationen, die auf die gleiche Anzahl an Kapazitaeten kommen, 
    # werden entfernt, sodass nur eine davon genutzt wird
    def bereinigeArray(self, array):
        oldSum = 0
        arrayBereinigt = []
        summen = []
        for kombi in array:
            sum = self.calcSum(kombi)
            if sum not in summen:
                arrayBereinigt.append(kombi)
                summen.append(sum)
        return arrayBereinigt, summen
    
    # Loese die Quest, Aufruf der Funktionen zum Loesen nach Kapazitaet / Zeit
    def trySolveIdea(self, array, kombis, zahl, kapazitaet, zeit):
        # sortiere die Kombinationen
        kombis = self.sortiereArray(kombis)
        print("Kombinationen:\t\t",kombis[1])
        
        # berechne Anzahl an Runden die fuer zeitkrit. maximal benoetigt werden wuerden
        sum = self.calcSum(array)
        rounds = zahl / sum
        roundGerundet = round(rounds)
        # runde auf
        if roundGerundet < rounds:
            rounds = roundGerundet + 1
        else:
            rounds = roundGerundet
        
        solution = [] 
        # falls Zeit gewaehlt
        if zeit:
            # berechne Loesung
            solutionZeit = self.calcSolutions(zahl, kombis[1], rounds, time.time())
            roundSolutions = []
            for r in range(0, rounds):
                # suche in den Kombinationen nach der jeweiligen Rundenkombi
                index = kombis[1].index(solutionZeit[r])
                # fuege die einzelnen Schiffe fuer die Runde hinzu
                roundSolutions.append(kombis[0][index])
            solution.append(roundSolutions)
            
        # falls Kapazitaet gewaehlt   
        if kapazitaet:
            solutionKap = self.trySolveKapazitaet(zahl, kombis[1], time.time())
            roundSolutions = []
            for r in range(0, len(solutionKap)):
                index = kombis[1].index(solutionKap[r])
                roundSolutions.append(kombis[0][index])
            solution.append(roundSolutions)
        return solution
    
    # finde die Loesung fuer kapazitaetskritisch
    def trySolveKapazitaet(self, zahl, array, startTime):
        numbers = array
        lastSolution = []
        actualSolution = []
        sumActual = 0
        actualTime = startTime
        # jede Kombination wird durchgegangen
        for k in range (self.getNextBiggerNumber(zahl, array), len(numbers)):
            actualSolution.append(numbers[k])
            # falls Kapazitaet noch nicht ausreicht, berechne fuer Runde 2 die beste
            # Wahl der Kombinationen
            if (zahl-numbers[k] > 0):
                finalSolution = self.trySolveKapazitaet((zahl-numbers[k]), array, actualTime)
                for f in finalSolution:
                    actualSolution.append(f)
            else:
                finalSolution = actualSolution
            # solange eine Loesung gefunden wurde:
            if not finalSolution == None:
                # berechne die Summe der Kapazitaeten und die Differenz
                # zu den Ressourcen, die losgeschickt werden
                sumFinal = self.calcSum(actualSolution)
                def1 = sumFinal-zahl
                def2 = sumActual-zahl
                # ist es die erste Loesung oder ist die gefundene Loesung
                # besser als die vorherige, speicher sie ab
                if (def1) <= (def2) or sumActual == 0:
                    if (def1 == def2):
                        # ist sie gleich gut, speicher die ab, mit den 
                        # wenigeren benoetigten Runden
                        if (len(actualSolution) < len(lastSolution)):
                            lastSolution = actualSolution.copy()
                            sumActual = sumFinal
                    else:
                        lastSolution = actualSolution.copy()
                        sumActual = sumFinal
            # loesche die aktuelle Loesung und pruefe die naechste
            if len(actualSolution) > 0:
                actualSolution.clear()
            if (time.time() - actualTime) > 10:
                return lastSolution
        return lastSolution     
    
    # finde die naechst groessere Zahl in einem Array
    def getNextBiggerNumber(self, zahl, array):
        finalIndex = 0
        for i in range (1, len(array)):
            if array[i] >= zahl:
                finalIndex = i
        return finalIndex
    
    # BubbleSort, koennte mit anderem Sortiertalgorithmus zum
    # Optimieren ausgewechselt werden
    def sortiereArray(self, kombis):
        for i in range (1, len(kombis[0])):
            for j in range (1, len(kombis[0])):
                if kombis[1][j-1] < kombis[1][j]:
                    self.swap(kombis[0], j-1, j)
                    self.swap(kombis[1], j-1, j)
        return kombis
    
    # tausche zwei Werte in einem Array
    def swap(self, array, i1, i2):
        old = array[i1]
        array[i1] = array[i2]
        array[i2] = old
    
    # finde die Loesung fuer zeitkritisch
    def calcSolutions(self, zahl, array, rounds, startTime):
        numbers = array
        lastSolution = []
        actualSolution = []
        sumActual = 0
        actualTime = startTime
        # selbes Prinzip wie kapazitaetskritisch, nur mit dem Unterschied:
        for k in range (0, len(numbers)):
            # es wird abgebrochen, wenn die Rundenanzahl nicht eingehalten werden kann
            if numbers[k]*rounds >= zahl:
                actualSolution.append(numbers[k])
                # und es wird auf die maximale Rundenanzahl begrenzt
                if (rounds > 1):
                    finalSolution = self.calcSolutions((zahl-numbers[k]), array, rounds-1, actualTime)
                    for f in finalSolution:
                        actualSolution.append(f)
                else:
                    finalSolution = actualSolution
                if not finalSolution == None:
                    sumFinal = self.calcSum(actualSolution)
                    if (sumFinal-zahl) < (sumActual-zahl) or sumActual == 0:
                        lastSolution = actualSolution.copy()
                        sumActual = sumFinal
            if len(actualSolution) > 0:
                actualSolution.clear()
            if ( time.time() - actualTime) > 10:
                return lastSolution
        return lastSolution          
                    
    # addiere alle Werte in einem Array und gebe die Summe zurueck
    def calcSum(self, array):
        sum = 0
        for a in array:
            sum += a
        return sum
