# -*- coding: utf-8 -*-
"""
Spyder Editor

Dies ist eine temporäre Skriptdatei.
"""

import sys

# QT stuff
from PyQt5.QtCore import QSize
from PyQt5.QtGui import QBrush, QImage, QPalette, QFont
from PyQt5.QtWidgets import QApplication, QGridLayout, QLabel, QLineEdit, QPushButton, QWidget

# database connection
from TestDBConnection import DBConnection

# menu widget
from menue_seaPort import Menue

class Login(QWidget):

    def __init__(self):
        super().__init__()
        
        self.title = 'SeaPort - Toolsoftware'
        self.left = 50
        self.top = 50
        self.width = 640
        self.height = 125
    
        
        self.initUI()
        
    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        
        # background image
        self.resizeEvent(None)
    
        # Input
        user_label = QLabel()
        user_label.setText("Name: ")
        user_label.setFont(QFont("Times", 12, QFont.Bold))
        user_label.setStyleSheet("QLabel { color : white; }")
        self.user_input = QLineEdit()
        
        # buttons
        btn_login = QPushButton('Login')
        btn_login.clicked.connect(self.login)
        self.buttonGetStyle(btn_login)
        
        # layout
        self.layout = QGridLayout()
        self.setLayout(self.layout)
        self.layout.addWidget(user_label, 0, 0, 1, 2)
        self.layout.addWidget(self.user_input, 0, 2, 1, 10)
        self.layout.addWidget(btn_login, 0, 14, 1, 2)
        
        self.show()
        
    def login(self):
        self.connection = DBConnection(host="remotemysql.com",
            user="u5LhSJr9zQ",
            password="Mp7ha2LNS5",
            database="u5LhSJr9zQ",
            username=self.user_input.text())

        
        if not self.connection.userExists():
            self.user_add = QLabel()
            self.user_add.setText("User existiert nicht. Hinzufügen? ")
            self.user_add.setFont(QFont("Times", 10, QFont.Bold))
            self.btn_yes = QPushButton('Ja')
            self.buttonGetStyle(self.btn_yes)
            self.btn_yes.clicked.connect(self.addUser)
            self.btn_no = QPushButton('Nein')
            self.buttonGetStyle(self.btn_no)
            self.btn_no.clicked.connect(self.resetLogin)
            
            self.layout.addWidget(self.user_add)
            self.layout.addWidget(self.btn_yes, 1, 11, 1, 1)
            self.layout.addWidget(self.btn_no, 1, 14, 1, 1)
        
            
        else:
            self.menu = Menue(self.connection)
            self.hide()
            #self.close()
    
    def resizeEvent(self, a0):
        # background image
        oImage = QImage("images/background.jpg")
        sImage = oImage.scaled(QSize(self.frameGeometry().width(), self.frameGeometry().height()))
        palette = QPalette()
        palette.setBrush(10, QBrush(sImage))
        self.setPalette(palette)
    
    def buttonGetStyle(self, button):
        styleSheet = """
QPushButton {
    background-color: #51b9a5; 
    border-radius: 7px; 
    padding: 5px 35px 5px 35px; 
    border: None
}

QPushButton:hover {
    background-color: #64b5f6;
    color: #fff;
}

QPushButton:pressed {
    background-color: #bbdefb;
} """
        button.setFont(QFont("Times", 10))
        button.setStyleSheet(styleSheet)
        
    def addUser(self):
        self.connection.addUser()
        self.menu = Menue(self.connection)
        self.hide()
        #self.close()
        
    def resetLogin(self):
        self.user_add.close()
        self.btn_yes.close()
        self.btn_no.close()
        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Login()
    sys.exit(app.exec_())