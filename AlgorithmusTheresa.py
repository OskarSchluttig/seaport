# -*- coding: utf-8 -*-
"""
Created on Tue Oct 26 17:13:26 2021

@author: Theresa
"""
import itertools
import numpy
from Schiff import Schiff

def zeitkritisch(ships, ressources):
    text = ""
    capacityTotal = 0
    #calculate capacity of all ships
    for ship in ships:
        capacityTotal += ship.kapazitaet
        
    step = 1
    #as long as there are more ressources than the whole capacity -> take all ships
    while ressources > capacityTotal:
        text += "# Runde " + str(step) + "\n"
        for ship in ships:
            text += "Schiff: " + ship.name + " Ladung: " + str(ship.kapazitaet) +"\n"
            ressources -= ship.kapazitaet
        step += 1
        
    text += "# Runde " + str(step)+"\n"
    
    #find ship with minimum of unused spaces
    currentMin = capacityTotal - ressources
    currentMinShips = ships.copy()
    for i in range(0, len(ships) + 1):
        #get all subsets
        for subset in itertools.combinations(ships, i):
            currentDifference = getSubsetSum(subset) - ressources
            #found perfect ship
            if currentDifference == 0:
                currentMin = 0
                currentMinShips = subset
                break
            #found ship with less wasted spaces
            elif currentDifference > 0 and currentDifference < currentMin:
                currentMin = currentDifference
                currentMinShips = subset
                continue
    
    #add ships to string
    for ship in currentMinShips:
        text += "Schiff: " + str(ship.name) + " Ladung: " + str(ship.kapazitaet)+"\n"
        ressources -= ship.kapazitaet
        
    #how many spaces are unused?        
    text += "Verschenkte Plätze: " + str(currentMin)+"\n"
    return text
                     
def getSubsetSum(array):
    sum = 0
    #sum of all elements in array
    for i in range(0, len(array)):
        sum += array[i].kapazitaet
    return sum

def kapazitaetskritisch(ships, ressources):
    #general method to call the recursive one
    [text, lostSpaces] = kapazitaetskritischRekursiv(ships, ressources, "")  
    text += "Verschenkte Plätze: " + str(lostSpaces) + "\n"
    return text

def kapazitaetskritischRekursiv(ships, ressources, text):
    perfectShip = None
    for ship1 in ships:
        #check if there's a ship which has multiple of ressources as capacity
        if ressources % ship1.kapazitaet == 0:
            perfectShip = ship1
            biggerShip = None
            for ship2 in ships:
                #check if there's a multiple of our perfect ship
                if ship2 != perfectShip and ship2.kapazitaet > perfectShip.kapazitaet and ship2.kapazitaet % perfectShip.kapazitaet == 0:
                    biggerShip = ship2
                    break
            if biggerShip == None:
                #no biggerShip found -> use only this ship
                deliveriesNr = ressources // perfectShip.kapazitaet
                for i in range(deliveriesNr):
                    text += "Schiff: " + perfectShip.name + " Ladung: " + str(perfectShip.kapazitaet) + "\n"
                return [text, 0]
            else:
                #bigger ship found --> use bigger as often as possible
                deliveriesBigShip = ressources // biggerShip.kapazitaet
                ressources -= deliveriesBigShip * biggerShip.kapazitaet
                #use perfectShip for the lasting deliveries
                deliveriesPerfectShip = ressources // perfectShip.kapazitaet
                deliveriesNr = max(deliveriesBigShip, deliveriesPerfectShip)
                #add lines to string
                for i in range(deliveriesNr):
                    if(i < deliveriesBigShip):
                        text += "Schiff: " + biggerShip.name + " Ladung: " + str(biggerShip.kapazitaet) + "\n"
                    if(i < deliveriesPerfectShip):
                        text += "Schiff: " + perfectShip.name + " Ladung: " + str(perfectShip.kapazitaet) + "\n"
                return [text, 0]
            
        else:
            #find next biggest ship
            difference = numpy.inf
            curMaxShip = None
            for ship in ships:
                curDiff = ressources - ship.kapazitaet
                #found ship with smaller difference
                if curDiff > 0 and curDiff < difference:
                   difference = curDiff
                   curMaxShip = ship
            if difference > 0 and curMaxShip != None:
                #recoursive call of function
                text += "Schiff: " + curMaxShip.name + " Ladung: " + str(curMaxShip.kapazitaet) + "\n"
                ressources -= curMaxShip.kapazitaet
                return kapazitaetskritischRekursiv(ships, ressources, text)
            else:
                #for last iteration find the min necessary capacity
                difference = -numpy.inf
                curMinShip = None
                for ship in ships:
                    curDiff = ressources - ship.kapazitaet
                    #found smaller difference (bigger but <0)
                    if curDiff > difference:
                        difference = curDiff
                        curMinShip = ship
                text += "Schiff: " + curMinShip.name + " Ladung: " + str(curMinShip.kapazitaet) + "\n"
                return [text, curMinShip.kapazitaet - ressources]
            
            