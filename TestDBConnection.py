# -*- coding: utf-8 -*-
"""
Created on Tue Sep  7 16:44:33 2021

@author: Oskar
"""

import mysql.connector
from Schiff import Schiff

class DBConnection:
    def __init__(self, host, user, password, database, username):
        self.user = user
        self.username = username
        self.db = mysql.connector.connect(
            host=host,
            user=user,
            password=password,
            database=database
        )
        self.cursor = self.db.cursor()
    
    def getSchiffe(self):
        self.cursor.execute('SELECT * FROM t_schiffe WHERE user="' + self.username + '"')
        schiffe = []
        for x in self.cursor.fetchall():
            schiffe.append(Schiff(*x))
        return schiffe
    
    def addSchiff(self, schiff):
        statement = "INSERT INTO t_schiffe (name, kapazitaet, user) VALUES (%s, %s, %s)"
        values = (schiff.name, schiff.kapazitaet, self.username)
        self.cursor.execute(statement, values)
        self.db.commit()
        schiff.id = self.cursor.lastrowid
        schiff.user = self.username
        return schiff
    
    def editSchiff(self, schiff):
        statement = 'UPDATE t_schiffe SET name = %s, kapazitaet = %s WHERE id = "' + str(schiff.id) + '"'
        values = (schiff.name, schiff.kapazitaet)
        self.cursor.execute(statement, values)
        self.db.commit()
    
    def removeSchiff(self, schiff_id):
        self.cursor.execute('DELETE FROM t_schiffe WHERE user = "' + str(self.username) + '" AND id = "' + str(schiff_id) + '"')
        self.db.commit()
        
    def addUser(self):
        statement = 'INSERT INTO t_user (name) VALUES ("' + self.username + '")'
        self.cursor.execute(statement)
        self.db.commit()
        
    def userExists(self):
        self.cursor.execute('SELECT * FROM t_user WHERE name="' + self.username + '"')
        if len(self.cursor.fetchall())==0:
            return False
        else:
            return True
        