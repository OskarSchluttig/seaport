import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QIcon, QImage, QPalette, QBrush, QFont, QColor
from PyQt5.QtCore import QSize, Qt

import AlgorithmusJeremy
from calcLucy import AlgorithmusLucy
from AlgorithmusConstanze import AlgorithmusConstanze
from AlgorithmusTheresa import zeitkritisch, kapazitaetskritisch
from AlgorithmOskar import AlgorithmOskar


class Quest(QWidget):

    def __init__(self, menu):
        super().__init__()

        self.title = 'SeaPort - Quests'
        # self.left = 10
        # self.top = 10
        # self.width = 640
        # self.height = 480
        self.zurueck = False
        self.menu = menu
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(*self.menu.geometry().getRect())

        # background image
        self.resizeEvent(None)

        # title
        self.titleLabel = QLabel("Quests", self)
        self.titleLabel.setFont(QFont("Times", 30, QFont.Bold))
        self.titleLabel.setAlignment(Qt.AlignCenter)
        self.titleLabel.setStyleSheet("color:black")
        self._addShadowEffect(self.titleLabel)

        # Input
        self.headline_input = QLabel()
        self.headline_input.setText("Eingabe der Mengen:")
        self.holz_input = QLineEdit("0")
        self.stahl_input = QLineEdit("0")
        self.fisch_input = QLineEdit("0")

        # Dropdown
        self.dropdown = QComboBox()
        self.dropdown.addItems(["Wähle Person", "Lucy", "Theresa", "Oskar", "Jeremy", "Constanze"])

        # Output
        self.headline_output = QLabel()
        self.headline_output.setText("Berechnete Lösung:")
        self.output = QPlainTextEdit()
        self.output.setReadOnly(True)

        # FormLayout
        self.flo = QFormLayout()
        self.flo.addWidget(self.titleLabel)
        self.flo.addWidget(self.headline_input)
        self.flo.addRow("Holz", self.holz_input)
        self.flo.addRow("Stahl", self.stahl_input)
        self.flo.addRow("Fisch", self.fisch_input)
        self.checkKapazitaet = QCheckBox('kapazitätskritisch')
        self.flo.addWidget(self.checkKapazitaet)
        self.checkZeit = QCheckBox('zeitkritisch')
        self.flo.addWidget(self.checkZeit)
        self.flo.addWidget(self.dropdown)

        # Button Berechne
        buttonBerechne = QPushButton('Berechne')
        self.menu.buttonGetStyle(buttonBerechne)
        self.flo.addWidget(buttonBerechne)
        buttonBerechne.clicked.connect(self.waehleAlgorithmus)

        self.flo.addWidget(self.headline_output)
        self.flo.addWidget(self.output)
        self.button_zurueck = QPushButton('Zurück zum Hauptmenü')
        self.menu.buttonGetStyle(self.button_zurueck)
        self.flo.addWidget(self.button_zurueck)
        self.button_zurueck.clicked.connect(self.setZurueck)

        self.setLayout(self.flo)
        self.show()

    def waehleAlgorithmus(self):
        name = self.dropdown.currentText()
        solution = ""
        ressourcen = []
        ressourcen.append(int(self.holz_input.text()))
        ressourcen.append(int(self.stahl_input.text()))
        ressourcen.append(int(self.fisch_input.text()))
        ships = self.menu.dbc.getSchiffe()
        if name == "Constanze":
            cr = AlgorithmusConstanze(sum(ressourcen), ships)
            solution += cr.getSchiffBestand()
            if self.checkKapazitaet.isChecked():
                solution += cr.kapazitaetkritischGesamt(ressourcen[0], ressourcen[1], ressourcen[2])
            if self.checkZeit.isChecked():
                solution += cr.zeitkritisch()

        if name == "Lucy":
            calculator = AlgorithmusLucy()
            kap = self.checkKapazitaet.isChecked()
            zeit = self.checkZeit.isChecked()
            solution = calculator.solveProblem(kap, zeit, ressourcen, self.menu.dbc)
        if name == "Theresa":
            ressourcesSum = sum(ressourcen)
            if self.checkZeit.isChecked():
                if self.checkKapazitaet.isChecked():
                    solution += "ZEITKRITISCH: \n" + zeitkritisch(ships,
                                                                  ressourcesSum) + "\n KAPAZITAETSKRITISCH: \n" + kapazitaetskritisch(
                        ships, ressourcesSum)
                else:
                    solution = "ZEITKRITISCH: \n" + zeitkritisch(ships, ressourcesSum)
            else:
                solution = "KAPAZITAETSKRITISCH: \n" + kapazitaetskritisch(ships, ressourcesSum)
        if name == "Jeremy":
            if self.checkZeit.isChecked():
                if self.checkKapazitaet.isChecked():
                    solution += "ZEITKRITISCH: \n" + AlgorithmusJeremy.solve_fast(ships,ressourcen) + "\n KAPAZITAETSKRITISCH: \n" + AlgorithmusJeremy.solve_capacity(ships, ressourcen)
                else:
                    solution += "ZEITKRITISCH: \n" + AlgorithmusJeremy.solve_fast(ships, ressourcen)
            else:
                solution += "KAPAZITAETSKRITISCH: \n" + AlgorithmusJeremy.solve_capacity(ships, ressourcen)
        if name == "Oskar":
            algo = AlgorithmOskar(ships)
            solution += algo.solve(sum(ressourcen),
                                   self.checkZeit.isChecked(),
                                   self.checkKapazitaet.isChecked())
        typ = ""
        if self.checkKapazitaet.isChecked():
            typ = "Kapazitaet"
        if self.checkZeit.isChecked():
            if not typ == "":
                typ += ", "
            typ += "Zeit"
        text = "Der Algorithmus von " + name + " wurde genutzt. \n Es wurde Wert gelegt auf: " + typ + "."
        text += "\n"
        text += solution
        self.output.setPlainText(text)

    def setZurueck(self):
        self.zurueck = True
        self.menu.show()
        self.hide()

    def _addShadowEffect(self, item):
        effect = QGraphicsDropShadowEffect()
        effect.setBlurRadius(1)
        effect.setColor(QColor("white"))
        effect.setOffset(0, 1)
        item.setGraphicsEffect(effect)

    def resizeEvent(self, a0):
        # background image
        oImage = QImage("images/background_quests.jpg")
        sImage = oImage.scaled(QSize(self.frameGeometry().width(), self.frameGeometry().height()))
        palette = QPalette()
        palette.setBrush(10, QBrush(sImage))
        self.setPalette(palette)
