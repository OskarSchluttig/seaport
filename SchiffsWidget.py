# -*- coding: utf-8 -*-
"""
Created on Mon Sep  6 12:01:51 2021

@author: Oskar
"""
import sys
import os
from PyQt5.QtWidgets import QWidget, QSizePolicy, QSpacerItem, QApplication, QHBoxLayout, QVBoxLayout, QPushButton, QLabel, QLineEdit, QStyle
from PyQt5.QtGui import QIcon, QFont, QImage, QPalette, QBrush
from PyQt5.QtCore import Qt, QSize
from TestDBConnection import DBConnection
from Schiff import Schiff

scriptDir = os.path.dirname(os.path.realpath(__file__))

class SchiffsWidget(QWidget):
    def __init__(self, dbc):
        super(SchiffsWidget, self).__init__()
        
        self.dbc = dbc
        
        lbl_name = QLabel("Name:")
        self.txt_name = QLineEdit()
        lbl_capacity = QLabel("Kapazität:")
        self.txt_capacity = QLineEdit("100")
        
        self.btn_save = QPushButton(self.style().standardIcon(getattr(QStyle, "SP_DialogApplyButton")), "")
        self.btn_save.clicked.connect(self.save)
        self.btn_edit = QPushButton(QIcon(scriptDir + os.path.sep + "icons" + os.path.sep + 'edit.svg'), "")
        self.btn_edit.clicked.connect(self.edit)
        self.btn_delete = QPushButton(self.style().standardIcon(getattr(QStyle, "SP_DialogCancelButton")), "")
        self.btn_delete.clicked.connect(self.delete)
        
        layout = QHBoxLayout()
        layout.addWidget(lbl_name)
        layout.addWidget(self.txt_name)
        layout.addWidget(lbl_capacity)
        layout.addWidget(self.txt_capacity)
        layout.addWidget(self.btn_save)
        layout.addWidget(self.btn_edit)
        layout.addWidget(self.btn_delete)
        
        self.setLayout(layout)
        
    def save(self):
        self.btn_save.setEnabled(False)
        self.txt_name.setEnabled(False)
        self.txt_capacity.setEnabled(False)
        if not hasattr(self, "schiff"):
            self.schiff = self.dbc.addSchiff(Schiff(name = self.txt_name.text(), kapazitaet = int(self.txt_capacity.text())))
        else:
            self.schiff.name = self.txt_name.text()
            self.schiff.kapazitaet = int(self.txt_capacity.text())
            self.dbc.editSchiff(self.schiff)
    
    def edit(self):
        self.btn_save.setEnabled(True)
        self.txt_name.setEnabled(True)
        self.txt_capacity.setEnabled(True)
    
    def delete(self):
        if hasattr(self, "schiff"):
            self.dbc.removeSchiff(self.schiff.id)
        self.close()

class SchiffsMask(QWidget):
    def __init__(self, menu):
        super(SchiffsMask, self).__init__()
        self.menu = menu
        self.schiffe = menu.dbc.getSchiffe()
        
        self.title = "SeaPort - Schiffe"
        self.initUI()
    
    def initUI(self):
        
        self.setWindowTitle(self.title)
        self.setGeometry(*self.menu.geometry().getRect())

        # background image
        self.resizeEvent(None)

        # title
        titleLabel = QLabel("Schiffe", self)
        titleLabel.setFont(QFont("Times", 30, QFont.Bold))
        titleLabel.setAlignment(Qt.AlignCenter)
        titleLabel.setStyleSheet("color:black")
        self.menu._addShadowEffect(titleLabel)
        
        layout = QVBoxLayout()
        layout.addWidget(titleLabel)
        
        def addSchiffsWidget():
            layout.insertWidget(1, SchiffsWidget(self.menu.dbc))
            return
        
        btn_add = QPushButton("Neues Schiff hinzufügen")
        self.menu.buttonGetStyle(btn_add)
        btn_add.clicked.connect(addSchiffsWidget)
        
        btn_exit = QPushButton("Zurück zum Hauptmenü")
        self.menu.buttonGetStyle(btn_exit)
        btn_exit.clicked.connect(self.backToMenu)
        
        spacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        
        layout.addWidget(btn_add)
        layout.addItem(spacer)
        layout.addWidget(btn_exit)

        
        for schiff in self.schiffe:
            w = SchiffsWidget(self.menu.dbc)
            w.schiff = schiff
            w.txt_name.setText(schiff.name)
            w.txt_capacity.setText(str(schiff.kapazitaet))
            w.btn_save.setEnabled(False)
            w.txt_name.setEnabled(False)
            w.txt_capacity.setEnabled(False)
            
            layout.insertWidget(1, w)
        
        self.setLayout(layout)  
    
    def backToMenu(self):
        self.menu.show()
        self.close()
    
    def resizeEvent(self, a0):
        # background image
        oImage = QImage("images/background.jpg")
        sImage = oImage.scaled(QSize(self.frameGeometry().width(), self.frameGeometry().height()))
        palette = QPalette()
        palette.setBrush(10, QBrush(sImage))
        self.setPalette(palette)

if __name__ == '__main__':
    app = QApplication([])
    dbc = DBConnection(host="sql11.freesqldatabase.com",
            user="sql11440421",
            password="BkDrvMUNFB",
            database="sql11440421",
            username="test123")
    ex = SchiffsMask(dbc)
    ex.show()
    sys.exit(app.exec_())